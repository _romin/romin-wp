<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'romin' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

// define( 'WP_HOME', 'https://0e7cd9fd8343.ngrok.io' );
// define( 'WP_SITEURL', 'https://0e7cd9fd8343.ngrok.io' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '=XcQ_Y!.MMBl(^R[?kJo>a45}!SVQQa8YLTiKFIC!}e8D;p(37T/JbWI7?Nl>U{c' );
define( 'SECURE_AUTH_KEY',  '0KF?_q#mW$vsEzl4[~^~hhli7:w~+vba?cN0FkTt.%uewllr,q64f<#5lAm]V^2=' );
define( 'LOGGED_IN_KEY',    'Rki.NF|A)t~f%qvh;KY;L[F, --|xH]}MYNA>!{9WYAc#.)ooNi`XLAC*wXBp6>J' );
define( 'NONCE_KEY',        '2ttW8S-PL|@*yX15z^P5nne-8;Yr4RsjJ?Hb,Z#N}B3?!yvvNvJw5v(W`M*LyiPS' );
define( 'AUTH_SALT',        '+Ug]yNd(iUaq!H|6d%8Utf$==4?,^GV{McFu1|/z9R99PKm[I(b^]=!e}ww&jOZ^' );
define( 'SECURE_AUTH_SALT', 'F=#q,{IhhI/DiX6Mut#3kLFo!h1(5tQ<8_I*jyP&x+#A9<yl=Fk@bNz1yKL;@]|^' );
define( 'LOGGED_IN_SALT',   'U,C-C<kR<!EHUpA.>aX9lhN8Za)?96=?PHme}H_<2H7tq*V$eN<|SI8=dt|0PF+Z' );
define( 'NONCE_SALT',       ' &,;r.zktaUamN-I+97i/:*ZYbPQU*&0GCCUUxw,(yiJj@+J;k>QL,{(a1 v>M4|' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
