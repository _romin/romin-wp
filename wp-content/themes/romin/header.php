<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width" />
        <title><?php bloginfo('name'); ?> <?php is_front_page() ? ' | ' . bloginfo('description') : wp_title(''); ?></title>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;500&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.typekit.net/eoh2lmy.css">
        <?php wp_head(); ?>
    </head>
<body class="themes">

    <?php if(is_home()): ?>
        <div class="splash">
            <div class="splash__page splash__page--default"></div>
            <div class="splash__page splash__page--pink"></div>
            <div class="splash__page splash__page--purple"></div>
            <div class="splash__page splash__page--blue"></div>
            <div class="splash__page splash__page--green"></div>
        </div>
    <?php endif; ?>

    <div class="circle"></div>
    <div class="circle__trail"></div>

    <div class="header">
        <div class="header__container">
            <a href="<?php echo home_url(); ?>" class="header__logo">
                Romin.
                <div class="header__logo--small">
                    dev
                </div>
            </a>
            <nav class="nav">
                <a class="nav__link" href="mailto:92.romin@gmail.com">contact</a>
                <a class="nav__link nav__link--scroll" href="#projects">projects</a>
            </nav>
        </div>
    </div>

    <div data-scroll-container>
        <div class="spacer"></div>