<?php get_header('secondary');  ?>
    <div class="project">
        <div class="project__overlay <?php if(get_field('project_colour')): ?> project__overlay--<?php echo get_field('project_colour'); ?>" <?php endif; ?>></div>

        <div class="project--fade-in">
            <?php get_template_part('flexible-content'); ?>
        </div>

    </div>
<?php get_footer('secondary'); ?>