<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width" />
        <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;500&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.typekit.net/eoh2lmy.css">
        <?php wp_head(); ?>
    </head>
<body>

    <div class="circle"></div>
    <div class="circle__trail"></div>

    <div class="header">
        <div class="header__container header__container--full-height">
            <a href="<?php echo home_url(); ?>" class="header__logo">
                <span>Romin.</span>
                <div class="header__logo--small">
                    <span>dev</span>
                </div>
            </a>
        </div>
    </div>

    <div data-scroll-container>
        <div class="spacer"></div>