import { TweenLite } from 'gsap';

export class Cursor {

    /**
     * @param {Object} props 
     */
    constructor(props) {
        this._props = props;

        if (!this.propsRequired(['target', 'speed'])) {
            return false;
        }

        this.onHover();
        window.addEventListener('mousemove', this.moveCursor.bind(this));
    }

    /**
     * Check required properties.
     * 
     * @param {Array} keys 
     */
    propsRequired(keys) {
        return keys.every(prop => this._props.hasOwnProperty(prop));
    }

    /**
     * Adjust cursor size when hovering over
     * an anchor tag.
     */
    onHover() {
        const links = document.getElementsByTagName('a');

        for (let i = 0; i < links.length; i++) {
            links[i].addEventListener('mouseover', this.growCursor.bind(this));
            links[i].addEventListener('mouseout', this.shrinkCursor.bind(this));
        }
    }

    /**
     * When moving the cursor on the page.
     * 
     * @param {Object} e 
     */
    moveCursor(e) {
        let props = this._props;

        TweenLite.to(props.target, props.speed, {
            x: e.clientX,
            y: e.clientY,
        });

        TweenLite.to(`${props.target}__trail`, (props.speed + 0.5), {
            x: e.clientX,
            y: e.clientY,
        });
    }

    /**
     * Increase the cursor size.
     */
    growCursor() {
        let props = this._props

        TweenLite.to(props.target, props.speed, {
            opacity: 1,
            scale: 0,
        });

        TweenLite.to(`${props.target}__trail`, (props.speed + 0.5), {
            scale: 3,
        });
    }

    /**
     * Reduce / reset the cursor size.
     */
    shrinkCursor() {
        let props = this._props;

        TweenLite.to(props.target, props.speed, {
            opacity: 1,
            scale: 1,
        });

        TweenLite.to(`${props.target}__trail`, (props.speed + 0.5), {
            scale: 1,
        });
    }
}
