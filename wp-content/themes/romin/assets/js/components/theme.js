/**
 * Themes.
 */

export class Theme {

    constructor() {

        const switcher = document.getElementById('theme-switcher');

        if (switcher) {
            const icon = switcher.children[0];

            switcher.addEventListener('click', () => {

                const body = document.getElementsByTagName('body')[0];

                if (document.querySelector('.themes__dark') !== null) {
                    document.cookie = "theme=themes";
                } else {
                    document.cookie = "theme=themes__dark";
                }

                body.classList.toggle('themes__dark');

                icon.classList.toggle('far');
                icon.classList.toggle('fas');
            });
        }
    }
}