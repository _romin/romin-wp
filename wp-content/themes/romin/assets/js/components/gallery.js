/**
 * Image Gallary.
 * 
 * On scroll move images left to right.
 */

import { gsap } from 'gsap/all';

export class ScrollGallery {

    constructor() {

        if (document.querySelector('.gallery')) {

            let gallery = document.querySelector('.gallery').clientWidth

            gsap.to('.gallery__item', {
                x: -gallery,
                scrollTrigger: {
                    trigger: '.gallery',
                    scroller: '[data-scroll-container]',
                    start: 'top-=40% 90%',
                    end: 'bottom+=40% 10%',
                    scrub: 1,
                },
            })
        }
    }
}