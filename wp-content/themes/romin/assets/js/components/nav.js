/**
 * Navigation.
 * 
 * Split text skew heading.
 */

import { TweenMax, Elastic } from 'gsap/all';

export class Nav {

    constructor() {

        let delay = 2;

        if (document.querySelector('.splash') !== null) {
            delay = 4.5
        }

        TweenMax.staggerFrom('.nav__link', 2, {
            scale: 0.5,
            delay: delay,
            opacity: 0,
            ease: Elastic.easeOut,
        }, 0.2);

        const links = document.querySelectorAll('.nav__link--scroll');

        links.forEach((link) => {
            link.addEventListener('click', this.scrollToSection);
        });
    }

    scrollToSection(e) {
        e.preventDefault();
        const href = this.getAttribute('href').replace('#', '.');
        const { offsetTop } = document.querySelector(`${href}`);

        locomotive.scrollTo(offsetTop);
    }
}