/**
 * Footer.
 * 
 * Fade in animation.
 */

import { TweenMax } from 'gsap/all';

export class Footer {

    constructor() {

        const footer = document.querySelector('.footer');

        TweenMax.fromTo(footer, 1,
            { opacity: 0, y: 50 },
            { delay: 4.5, opacity: 1, y: 0 }
        );
    }
}