/**
 * Header Logo.
 * 
 * Logo split text animation.
 */

import { gsap, SplitText } from 'gsap/all';

export class Logo {

    constructor() {

        let delay = 2;

        if (document.querySelector('.splash') !== null) {
            delay = 4.5
        }

        var childSplit = new SplitText('.header__logo', {
            type: 'lines',
            linesClass: 'header__logo--split-child'
        });

        new SplitText('.header__logo', {
            type: 'lines',
            linesClass: "header__logo--split-parent"
        });

        gsap.from(childSplit.lines, {
            duration: 1.5,
            yPercent: 100,
            delay: delay,
            ease: 'power4',
            stagger: 0.1
        });
    }
}