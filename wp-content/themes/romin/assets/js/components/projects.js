/**
 * Navigation.
 * 
 * Split text skew heading.
 */

import { gsap, TweenMax, ScrollTrigger, Power4 } from 'gsap/all';

export class Projects {

    constructor() {
        gsap.delayedCall(5, this.loadProjects.bind(this))

        this.projectOverlay();
    }

    loadProjects() {
        gsap.utils.toArray('.project').forEach((section) => {

            const cover = section.querySelector('.project__cover');
            const information = section.querySelector('.information');

            ScrollTrigger.create({
                trigger: section,
                scroller: '[data-scroll-container]',
                onEnter: () => {

                    gsap.to(cover, {
                        scaleX: 0,
                        duration: 1.5,
                        scrollTrigger: section,
                        transformOrigin: 'right',
                        ease: Power4.easeInOut,
                        scroller: '[data-scroll-container]',
                        onStart: () => {

                            TweenMax.fromTo(information, 0.5,
                                { opacity: 0 },
                                { opacity: 1 });
                        }
                    });

                },
            });
        });
    }

    /**
     * Project Overlay.
     *
     * Slide in overlay left to right.
     */
    projectOverlay() {
        if (document.querySelector('.project__overlay')) {
            gsap.fromTo('.project__overlay', { scaleX: 0 }, {
                transformOrigin: 'left',
                duration: 1.75,
                scaleX: 1,
                ease: Power4.easeInOut,
            }, 0.25);

            gsap.delayedCall(2, () => {
                const content = document.querySelector('.project--fade-in');
                TweenMax.fromTo(content, 1, { opacity: 0, y: 50 }, { opacity: 1, y: 0 });
            });
        }
    }
}