/**
 * Splash Page.
 * 
 * Multiple coloured sections moving 
 * left to right.
 */

import { TimelineLite, Power4 } from 'gsap/all';

export class Splash {

    constructor() {

        const tl = new TimelineLite();

        let splashPageTime = 0;

        const colours = [
            'pink', 'purple', 'blue', 'green',
        ];

        colours.forEach((colour) => {
            if (document.querySelector(`.splash__page--${colour}`)) {
                tl.fromTo(`.splash__page--${colour}`, { scaleX: 0 }, {
                    transformOrigin: 'left',
                    duration: 2,
                    scaleX: 1,
                    ease: Power4.easeInOut,
                }, splashPageTime);

                splashPageTime += 0.25;
            }
        });

        colours.forEach((colour, i) => {
            if (document.querySelector(`.splash__page--${colour}`)) {
                if (i === colours.length - 1) {
                    tl.to(`.splash__page--${colour}`, {
                        scaleX: 0,
                        duration: 2.5,
                        transformOrigin: 'right',
                        ease: Power4.easeInOut,
                    });
                }

                tl.set(`.splash__page--${colour}`, { scaleX: 0 });
                tl.set('.splash__page--default', { scaleX: 0 });
            }
        });
    }
}