/**
 * Hero.
 * 
 * Split text skew heading.
 */

import { gsap, SplitText } from 'gsap/all';

export class Hero {

    constructor() {

        if (document.querySelector('.hero__heading')) {

            const heroSplitText = new SplitText('.hero__heading', {
                type: 'words,chars',
            });

            const { chars } = heroSplitText;

            gsap.set('.hero__heading', { perspective: 400 });
            gsap.from(chars, {
                duration: 1,
                delay: 4.5,
                opacity: 0,
                scale: 0,
                rotationX: 130,
                transformOrigin: '0% 50% -50',
                ease: 'back',
                stagger: 0.01,
            }, '+=0');
        }
    }
}