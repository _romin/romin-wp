/**
 * Required to compile main CSS file.
 */
import '../sass/main.scss';

import {
    gsap,
    TimelineLite,
    ScrollTrigger,
} from 'gsap/all';

gsap.registerPlugin(TimelineLite, ScrollTrigger);

import Marquee3k from 'marquee3000';
import MagnetMouse from 'magnet-mouse';
import LocomotiveScroll from 'locomotive-scroll';

Marquee3k.init();

let mm = new MagnetMouse({
    magnet: {
        element: '.magnet'
    }
}).init()

window.locomotive = new LocomotiveScroll({
    el: document.querySelector('[data-scroll-container]'),
    smooth: true,
    tablet: { smooth: false },
    smartphone: { smooth: false },
    getDirection: true,
});

import { helpers } from './helpers';
import { Cursor } from './cursor';
import { Scroll } from './scroll';

new Cursor({
    target: '.circle',
    speed: 0.3,
});

const scroll = new Scroll();

locomotive.on('scroll', (instance) => {
    if (instance.scroll.y > 0 && instance.scroll.y < 100) {
        document.documentElement.setAttribute('data-direction', instance.direction)
    }
});

import { Nav } from './components/nav';
import { Logo } from './components/logo';
import { Hero } from './components/hero';
import { Splash } from './components/splash';
import { Footer } from './components/footer';
import { Projects } from './components/projects';
import { ScrollGallery } from './components/gallery';
import { Theme } from './components/theme';

new Nav();
new Logo();
new Hero();
new Splash();
new Projects();
new Footer();
new ScrollGallery();
new Theme();

document.body.classList.add(helpers.getCookie('theme'));

scroll.scrollTriggerRefresh();