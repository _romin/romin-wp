import {
    ScrollTrigger,
} from 'gsap/all';


export class Scroll {

    constructor() {

        locomotive.on('scroll', (instance) => {
            ScrollTrigger.update
        });

        this.proxy();
    }

    proxy() {
        ScrollTrigger.scrollerProxy('[data-scroll-container]', {
            scrollTop(value) {
                return arguments.length ? locomotive.scrollTo(value, 0, 0) : locomotive.scroll.instance.scroll.y;
            },
            getBoundingClientRect() {
                return { top: 0, left: 0, width: window.innerWidth, height: window.innerHeight };
            },
            pinType: document.querySelector('[data-scroll-container]').style.transform ? "transform" : "fixed"
        });
    }

    scrollTriggerRefresh() {
        ScrollTrigger.addEventListener('refresh', () => locomotive.update());
        ScrollTrigger.refresh();
    }
}