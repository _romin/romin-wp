<?php

/**
 * Add image size.
 */
add_image_size( 'tall', 740, 760);
add_image_size( 'small', 630, 520);
add_image_size( 'wide', 830, 445);


/**
 * Add theme support.
 * 
 * @return void
 */
function theme_support() {

    add_theme_support('post-thumbnails');
    add_post_type_support( 'project', [
        'thumbnail'
    ] ); 
}

add_action('init', 'theme_support');


/**
 * Create post types.
 * 
 * @return void
 */
function create_post_types() {
 
    register_post_type( 'project',
        [
            'labels' => [
                'name' => __( 'Projects' ),
                'singular_name' => __( 'Project' )
            ],
            'public' => true,
            'has_archive' => true,
            'menu_icon'   => 'dashicons-text-page',
        ]
    );
}

add_action('init', 'create_post_types');

/**
 * Header scripts and stylesheet.
 *
 * @return void
 */
function header_scripts()
{
    wp_enqueue_style('stylesheet', get_stylesheet_directory_uri() . '/dist/main.css', '1.0.5', true);
    wp_enqueue_script('fontawesome', 'https://kit.fontawesome.com/1d83160788.js');
}

add_action('wp_enqueue_scripts', 'header_scripts');

/**
 * Footer scripts.
 *
 * @return void
 */
function footer_scripts()
{
    wp_enqueue_script('scripts', get_stylesheet_directory_uri() . '/dist/main.bundle.js', ['jquery'],  '1.0.3', true);
}

add_action('wp_footer', 'footer_scripts');

 
/**
 * Save ACF JSON
 *
 * @return string
 */
function save_acf_json( $path ) {
    
    $path = get_stylesheet_directory() . '/acf-json';
    return $path;
}

add_filter('acf/settings/save_json', 'save_acf_json');


/**
 * Load ACF JSON
 *
 * @return string
 */
function load_acf_json( $paths ) {
    
    unset($paths[0]);
    
    $paths[] = get_stylesheet_directory() . '/acf-json';
    
    return $paths;
}

add_filter('acf/settings/load_json', 'load_acf_json');