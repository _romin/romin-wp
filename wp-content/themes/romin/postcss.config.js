/**
 * PostCSS plugins.
 */
module.exports = {
    plugins: [
      require('autoprefixer'),
      require('postcss-responsive-type')
    ]
}