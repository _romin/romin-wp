<?php
    $projects = new WP_Query([
        'post_type' => 'project',
        'post_status' => 'publish',
        'order' => 'DESC',
        'posts_per_page' => -1
    ]);
?>

<?php if ($projects->have_posts()): ?>
    <div class="projects">
        <div class="projects__container">

            <?php while ( $projects->have_posts() ) :  $projects->the_post(); ?>
                <a href="<?php echo get_post_permalink() ?>" class="project <?php if(get_field('project_colour')): ?> project__colour--<?php echo get_field('project_colour'); ?> <?php endif; ?> project__<?php echo get_field('project_card_size'); ?>">
                    <img class="project__image" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), get_field('project_card_size')); ?>" alt="">
                    <div class="project__cover"></div>
                    <div data-scroll data-scroll-speed="-1">
                        <div class="information">
                            <div class="information__sub-heading">
                                <?php echo get_field('project_year'); ?>
                            </div>
                            <div class="information__heading">
                                <?php echo get_field('project_heading'); ?>
                            </div>
                        </div>
                    </div>
                </a>
            <?php endwhile; ?>
            
        </div>
    </div>
<?php endif; ?>
