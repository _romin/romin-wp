<div class="project__video <?php if(get_sub_field('vertical_align')): ?> project__video--align-<?php echo get_sub_field('vertical_align'); ?>  <?php endif; ?>">
    <video data-scroll data-scroll-speed="-1.5" loop autoplay muted playsinline>
        <source src="<?php echo get_sub_field('video'); ?>" type="video/mp4">
    </video>
</div>