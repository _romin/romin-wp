<div data-scroll data-scroll-speed="-1" class="project__information project__information--center project__information--large">
    <?php if(get_sub_field('heading')): ?>
        <h2><?php echo get_sub_field('heading'); ?></h2>
    <?php endif; ?>
    <?php if(get_sub_field('content')): ?>
        <?php echo get_sub_field('content'); ?>
    <?php endif; ?>
</div>