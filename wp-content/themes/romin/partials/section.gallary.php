<?php 
    $images = get_sub_field('gallery');
?>
<?php if( $images ): ?>
    <div class='gallery'>
        <?php foreach( $images as $image ): ?>
            <div class="gallery__item">
                <div class="image" style="background-image: url(<?php echo $image; ?>)"></div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>