<div class="project__marquee">
    <?php $i = 0; ?>
    <?php if( have_rows('marquee') ): ?>
        <?php while( have_rows('marquee') ) : the_row(); ?>
            <div class="line" data-scroll data-scroll-direction="horizontal" data-scroll-speed="<?php if($i % 2 == 0): ?> -2 <?php else: ?> 2 <?php endif; ?>">
                <div class="marquee3k" data-speed="0.8" <?php if($i % 2 == 0): ?> data-reverse="true" <?php endif; ?>>
                    <h1><?php echo get_sub_field('line'); ?></h1>
                </div>
            </div>
            <?php $i++; ?>
        <?php endwhile; ?>
    <?php endif; ?>
</div>