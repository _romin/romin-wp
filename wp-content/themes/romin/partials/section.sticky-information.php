<div class="project__container">
    <div class="sticky" id="pinSection">

        <?php if(get_sub_field('heading')): ?>
            <div class="sticky__heading" data-scroll data-scroll-sticky data-scroll-target="#pinSection">
                <h1><?php echo get_sub_field('heading'); ?></h1>
            </div>
        <?php endif; ?>
       
        <?php if( have_rows('content') ): ?>
            <?php while( have_rows('content') ) : the_row(); ?>

                <div class="sticky__information">
                    <div class="sticky__information--list">
                        <?php if(get_sub_field('heading')): ?>
                            <h2>
                                <?php echo get_sub_field('heading'); ?>
                            </h2>
                        <?php endif; ?>
                        <?php if(get_sub_field('information')): ?>
                            <?php echo get_sub_field('information'); ?>
                        <?php endif; ?>
                    </div>
                </div>

            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>