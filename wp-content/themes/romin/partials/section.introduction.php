<div class="project__container">
    <div class="project__information">
        <?php if(get_sub_field('heading')): ?>
            <h1 class="h1"><?php echo get_sub_field('heading'); ?></h1>
        <?php endif; ?>
        <?php if(get_sub_field('content')): ?>
            <div class="lead">
                <?php echo get_sub_field('content'); ?>
            </div>
        <?php endif; ?>
    </div>
</div>