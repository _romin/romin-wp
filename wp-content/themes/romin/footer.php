
    </div>
    <div class="footer">
        <div class="footer__container">
            <div class="scroll wow animate__fadeInDown" data-wow-delay="4.25s">
                <div class="scroll__down">
                    See My Work
                    <span class="scroll__arrow wow animate__heartBeat"></span>
                </div>
            </div>
            <a href="javascript:void(0)" class="theme__switch wow animate__fadeInDown magnet" data-wow-delay="4.25s" id="theme-switcher">
                <i class="fas fa-lightbulb"></i>
            </a>
        </div>
    </div>
    <?php wp_footer();?>
</body>
</html>