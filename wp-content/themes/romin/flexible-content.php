<?php 
    if( have_rows('flexible_content') ):
        while ( have_rows('flexible_content') ) : the_row();
            switch ( get_row_layout() ) :

                case 'introduction':
                    get_template_part('partials/section.introduction');
                break;

                case 'sticky_information':
                    get_template_part('partials/section.sticky-information');
                break;

                case 'information':
                    get_template_part('partials/section.information');
                break;

                case 'video':
                    get_template_part('partials/section.video');
                break;

                case 'gallery':
                    get_template_part('partials/section.gallary');
                break;

                case 'marquee':
                    get_template_part('partials/section.marquee');
                break;
                
            endswitch;  
        endwhile;
    endif;
?>