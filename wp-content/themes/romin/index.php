<?php 
    /* Template Name: Homepage */
    get_header(); 
?>
    <div class="hero">
        <div class="hero__container">
        <div class="hero__heading hero__heading--tablet" data-scroll data-scroll-speed="-1.5">
                Hi, I am <br> <span class="hero__heading--lead">Romin Patel,</span> <br>
                a Web Developer.
            </div>

            <div class="hero__heading hero__heading--desktop" data-scroll data-scroll-speed="-1.5">
                Hi, I am <br> <span class="hero__heading--lead">Romin Patel,</span> <br>
                a Web Developer focused on
                crafting experiences for the
                internet users.
            </div>
        </div>
    </div>

    <?php get_template_part('partials/section.projects'); ?>
<?php get_footer(); ?>